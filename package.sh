# Packaging script
# DO NOT REMOVE OR MODIFY

echo 'packaging assets'
mv assets resourcepacks

echo 'packaging pack.mcmeta'
mv pack.mcmeta resourcepacks

echo 'searching for license file'
if [ -f LICENSE ]; then
  echo 'license found, packaging'
  mv LICENSE resourcepacks
fi

echo 'searching for resourcepack logo'
if [ -f pack.png ]; then
  echo 'logo found, packaging'
  mv pack.png resourcepacks
fi